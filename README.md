# Striemtrien

Striemtrien is a graphical user interface to start mpv media player without having to download videos first. Just paste a URL of a page with a video in it to play it directly!

## Installation
`git clone https://git.fsfe.org/jvs/Striemtrien.git`

`cd Striemtrien`

`sudo apt install mpv python3-gi python3-pip python3-setuptools python3-wheel`

`sudo -H pip3 install youtube-dl`

`./install.sh`

## Usage

Open the application, paste a link, click the button.

![Screenshot](https://git.fsfe.org/jvs/Striemtrien/raw/master/striemtrien-screenshot.png)

As the URL's of streaming sites change often, Striemtrien requires an up-to-date youtube-dl script. The best way is to use pip as described above.

youtube-dl can be updated with pip easily: `sudo -H pip3 install -U youtube-dl`

## License

`Expat` (MIT), see LICENSE
