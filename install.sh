#!/bin/sh

mkdir -p ~/.local/bin > /dev/null 2>&1
mkdir -p ~/.local/share/applications > /dev/null 2>&1
mkdir -p ~/.local/share/icons > /dev/null 2>&1

cp striemtrien.py ~/.local/bin
cp striemtrien.desktop ~/.local/share/applications
cp striemtrien.svg ~/.local/share/icons
