#!/usr/bin/env python3

'''
Copyright 2017 Justin van Steijn, Kevin Keijzer

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
'''

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
import subprocess
import time

class StriemTrien(object):
    
    def __init__(self, page_url, video):
        self.page_url = page_url
        self.video = video
        
    def start_mpv(self):
        mpv_command = [
                'mpv', 
                '--msg-level=all=error', 
                '--title=Striemtrien', 
                self.page_url,
                ]
        if self.video == False:
            mpv_command.insert(1, '--no-video')
            mpv_command.insert(2, '--force-window')
            mpv_command.insert(3, '--window-scale=0.5')
        self.process = subprocess.Popen(mpv_command, stdout=subprocess.PIPE)

        begintime = time.time()
        while (time.time() - begintime) < 3:
            returncode = self.process.poll()
            if returncode == 2:
                error = self.process.stdout.read().decode('utf-8')
                win.show_message(
                                "<b>No video could be found for the entered URL. Try again.</b>\n"
                                + error
                                )
                return
        Gtk.main_quit()

class StriemTrienWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="Striemtrien")

        self.set_size_request(500, 100)
        self.timeout_id = None
        
        vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=6)
        self.add(vbox)

        self.entry = Gtk.Entry()
        self.entry.set_text("")
        self.entry.connect("activate", self.on_button_clicked)
        vbox.pack_start(self.entry, True, True, 0)
        
        self.label = Gtk.Label()
        vbox.pack_start(self.label, True, True, 0)
        
        hbox = Gtk.Box(spacing=0)
        vbox.pack_start(hbox, True, True, 0)
        
        audio_button = Gtk.Button(label="Play audio only")
        hbox.pack_start(audio_button, False, False, 1)
        audio_button.connect("clicked", self.on_button_clicked, False)
        
        video_button = Gtk.Button(label="Play video")
        hbox.pack_start(video_button, True, True, 1)
        video_button.connect("clicked", self.on_button_clicked)
                
    def on_button_clicked(self, widget, video=True):
        page_url = self.entry.get_text()
        striemtrien = StriemTrien(page_url, video)
        striemtrien.start_mpv()
        
    def show_message(self, message):
        print(message)
        self.label.set_markup(message)

if __name__ == '__main__':
    
    win = StriemTrienWindow()
    win.connect("delete-event", Gtk.main_quit)
    win.set_wmclass("Striemtrien", "Striemtrien")
    win.set_title("Striemtrien")
    win.show_all()
    Gtk.main()
